package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/encoding/prototext"

	"gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjrpc/genpjrpc"
)

var (
	version = "v1.0.1"

	flags                  flag.FlagSet
	methodsWithServiceName = flags.Bool(
		"methods_with_service_name",
		false,
		"method name will be with service name prefix 'service_name_method_name'",
	)

	pathProtoRequest = flags.String(
		"path_proto_request",
		"",
		"path to save protobuf request, will not save the file if the value is empty",
	)
)

func run(gen *protogen.Plugin) error {
	genpjrpc.Version = version
	genpjrpc.MethodsWithServiceName = *methodsWithServiceName

	for _, f := range gen.Files {
		if !f.Generate {
			continue
		}

		genpjrpc.GenerateFile(gen, f)
	}

	if *pathProtoRequest == "" {
		return nil
	}

	file, err := os.Create(*pathProtoRequest)
	if err != nil {
		return fmt.Errorf("failed to create file for proto request: %w", err)
	}

	request, err := prototext.Marshal(gen.Request)
	if err != nil {
		return fmt.Errorf("prototext.Marshal: %w", err)
	}

	if _, err = file.Write(request); err != nil {
		return fmt.Errorf("file.Write: %w", err)
	}

	if err = file.Close(); err != nil {
		return fmt.Errorf("file.Close: %w", err)
	}

	return nil
}

func showVesrionsOrHelp() {
	if os.Args[1] == "--version" {
		fmt.Fprintf(os.Stderr, "%s %s\n", filepath.Base(os.Args[0]), version)
		return
	}

	flags.PrintDefaults()
}

func main() {
	if len(os.Args) == 2 {
		showVesrionsOrHelp()
		os.Exit(0)
	}

	o := protogen.Options{
		ParamFunc:         flags.Set,
		ImportRewriteFunc: nil,
	}

	o.Run(run)
}
