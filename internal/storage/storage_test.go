package storage_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/errs"
	"gitlab.com/pjrpc/pjrpc/internal/storage"
)

type handler func(route string)

// TestStorage tests that storage can put and return values.
func TestStorage(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	r1 := "route1"
	var h1 handler = func(route string) { so.Equal(route, r1) }

	r2 := "route2"
	var h2 handler = func(route string) { so.Equal(route, r2) }

	s := storage.New()

	err := s.Put(r1, h1)
	so.NoError(err)
	h, err := s.Get(r1)
	so.NoError(err)
	h.(handler)(r1)

	h, err = s.Get(r2)
	so.True(errors.Is(err, errs.ErrRouteNotFound))
	so.Nil(h)
	err = s.Put(r1, h2)
	so.True(errors.Is(err, errs.ErrRouteAlreadyExists))

	err = s.Put(r2, h2)
	so.NoError(err)
	h, err = s.Get(r2)
	so.NoError(err)
	h.(handler)(r2)
	h, err = s.Get(r1)
	so.NoError(err)
	h.(handler)(r1)

	s.Replace(r1, h2)
	h, err = s.Get(r1)
	so.NoError(err)
	h.(handler)(r2)
}
