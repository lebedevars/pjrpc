// +build wasm
package main

import (
	"context"
	"log"

	"gitlab.com/pjrpc/pjrpc/client"

	"wasm/proto"
)

func main() {
	cl, err := client.New("http://127.0.0.1:8080/rpc") // or just "/rpc" in browser
	if err != nil {
		log.Fatalf("failed to client.New: %s", err)
	}
	cl.SetLogger(log.Writer()) // Client can write errors about closing the response body.

	rpc := proto.NewWebAssemblyRPCClient(cl)

	resp, err := rpc.Procedure(context.Background(), &proto.Request{Field: "field"})
	if err != nil {
		log.Fatalf("failed to call Procedure: %s", err)
	}

	log.Println(resp.FieldOut)
	log.Println("exit")
}
