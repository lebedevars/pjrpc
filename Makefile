.PHONY: test coverage lints lints_fix example_proto

test:
	go test -cover ./...
	cd ./cmd/protoc-gen-go-pjrpc/ && make test
	cd ./cmd/protoc-gen-go-pjson/ && make test

coverage:
	go test -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml

example_proto:
	# generates test protobuf files
	cd _examples/helloworld && \
	protoc \
		--go-pjson_out=. --go-pjson_opt=paths=source_relative \
		--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
		proto/helloworld.proto
		
	cd _examples/middlewares && \
	protoc \
		--go-pjson_out=. --go-pjson_opt=paths=source_relative \
		--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
		proto/middlewares.proto
	
	cd _examples/wasm && \
	protoc \
		--go-pjson_out=. --go-pjson_opt=paths=source_relative \
		--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
		proto/wasm.proto
