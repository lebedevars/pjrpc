package ctxtool_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/ctxtool"
	"gitlab.com/pjrpc/pjrpc/model"
)

func TestContextMethods(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	ctx := context.Background()

	//
	// HTTP request
	//

	req, ok := ctxtool.GetHTTPRequest(ctx)
	so.False(ok)
	so.Nil(req)

	tReq := new(http.Request)
	ctx = ctxtool.SetHTTPRequest(ctx, tReq)

	req, ok = ctxtool.GetHTTPRequest(ctx)
	so.True(ok)
	so.Equal(tReq, req)

	//
	// JSON request
	//

	jReq, ok := ctxtool.GetJSONRPCRequest(ctx)
	so.False(ok)
	so.Nil(jReq)

	tJReq := new(model.Request)
	ctx = ctxtool.SetJSONRPCRequest(ctx, tJReq)

	jReq, ok = ctxtool.GetJSONRPCRequest(ctx)
	so.True(ok)
	so.Equal(tJReq, jReq)

	// IsBatch
	isBatch, ok := ctxtool.GetIsBatch(ctx)
	so.False(ok)
	so.False(isBatch)

	ctx = ctxtool.SetIsBatch(ctx, true)

	isBatch, ok = ctxtool.GetIsBatch(ctx)
	so.True(ok)
	so.True(isBatch)

	ctx = ctxtool.SetIsBatch(ctx, false)

	isBatch, ok = ctxtool.GetIsBatch(ctx)
	so.True(ok)
	so.False(isBatch)
}
