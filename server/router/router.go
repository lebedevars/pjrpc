// Package router provides methods to register and invoke request handlers.
package router

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/pjrpc/pjrpc/internal/storage"
)

// Handler request handler takes request context and raw json params.
// Returns type with result data and error.
type Handler func(ctx context.Context, params json.RawMessage) (interface{}, error)

// Middleware wraps handler and call before wrapped handler.
type Middleware func(next Handler) Handler

// Router contains storage with handlers and common middleware.
type Router struct {
	storage    *storage.Storage
	middleware Middleware
}

// New creates new router with empty handler storage.
func New() *Router {
	r := &Router{
		storage: storage.New(),
	}

	return r
}

// RegisterMethod saves handler of the method to starage.
// Panic if method with that name already exists.
func (r *Router) RegisterMethod(methodName string, h Handler) {
	err := r.storage.Put(methodName, h)
	if err != nil {
		panic("pjrpc: router: storage.Put: " + err.Error())
	}
}

// With adds middleware to router.
// Can be called multiple times. Each call adds middleware to the stack.
// The last registered middleware will be called first.
//
// Example:
//   r.With(last).With(preLast).With(middle).With(preFirst).With(first)
//
// "first" is the last to register and will be called first.
func (r *Router) With(mw Middleware) {
	if r.middleware == nil {
		r.middleware = mw
		return
	}

	lastMW := r.middleware
	r.middleware = func(next Handler) Handler {
		return mw(lastMW(next))
	}
}

// MethodWith adds middleware to registered handler.
// Can be called multiple times. Each call adds middleware to the stack.
// The last registered middleware will be called first.
// If the method is not registered there will be panic.
func (r *Router) MethodWith(methodName string, mw Middleware) {
	storedHandler, err := r.storage.Get(methodName)
	if err != nil {
		panic("pjrpc: router: MethodWith: storage.Get: " + err.Error())
	}

	r.storage.Replace(methodName, mw(storedHandler.(Handler)))
}

// Invoke invokes handler by method name.
// If the router has middleware, it will be called first.
// After that the registered middleware will be called and then the registered handler.
func (r *Router) Invoke(ctx context.Context, methodName string, params json.RawMessage) (interface{}, error) {
	storedHandler, err := r.storage.Get(methodName)
	if err != nil {
		return nil, fmt.Errorf("storage.Get: %w", err)
	}

	handler := storedHandler.(Handler) // nolint:errcheck,forcetypeassert // we put in storage Handler type only.
	if r.middleware != nil {
		handler = r.middleware(handler)
	}

	res, err := handler(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("method '%s': %w", methodName, err)
	}

	return res, nil
}
