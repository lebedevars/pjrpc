package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"
	"time"

	"gitlab.com/pjrpc/pjrpc/ctxtool"
	"gitlab.com/pjrpc/pjrpc/server"
	"gitlab.com/pjrpc/pjrpc/server/router"

	"middlewares/proto"
)

type ctxKey int

const (
	ctxKeyLogger ctxKey = iota + 1
)

type rpc struct{}

// timer should be called last before method handler.
func (r *rpc) timer(next router.Handler) router.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		now := time.Now()
		res, err := next(ctx, params)
		ctx.Value(ctxKeyLogger).(*log.Logger).Printf("execute method %.6f\n", time.Since(now).Seconds())
		return res, err
	}
}

// contextLogger should be called first to add logger in the context.
func (r *rpc) contextLogger(next router.Handler) router.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		ctx = context.WithValue(ctx, ctxKeyLogger, log.New(log.Writer(), "[ctx-logger] ", log.LstdFlags))
		log.Println("I'm add logger to context :)")
		return next(ctx, params)
	}
}

// requestDebugger just debugger called after logger.
func (r *rpc) requestDebugger(next router.Handler) router.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		req, _ := ctxtool.GetHTTPRequest(ctx)
		dump, err := httputil.DumpRequest(req, false)
		if err != nil {
			ctx.Value(ctxKeyLogger).(*log.Logger).Printf("failed to DumpRequest: %s\n", err)
			return next(ctx, params)
		}

		log.Println("request:", string(dump))
		return next(ctx, params)
	}
}

// personalWrapper wraps handler personally.
func (r *rpc) personalWrapper(next router.Handler) router.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		jReq, ok := ctxtool.GetJSONRPCRequest(ctx)
		if !ok {
			panic("ctxtool.GetJSONRPCRequest")
		}

		if jReq.Method == proto.JSONRPCMethodJustMethod {
			panic("wrong method")
		}

		log.Println("personalWrapper")

		return next(ctx, params)
	}
}

// JustMethod handler.
func (r *rpc) JustMethod(ctx context.Context, in *proto.HelloRequest) (*proto.HelloResponse, error) {
	log.Println("handler JustMethod:", in)

	return &proto.HelloResponse{Name: "just method"}, nil
}

// WrapPersonal handler.
func (r *rpc) WrapPersonal(ctx context.Context, in *proto.HelloRequest) (*proto.HelloResponse, error) {
	log.Println("handler WrapPersonal:", in)

	return &proto.HelloResponse{Name: "just method"}, nil
}

func main() {
	srv := server.New()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.

	r := &rpc{}

	// registers rpc with common middlewares.
	// last middleware in list will be called the first. (Stack)
	proto.RegisterMiddlewareTesterServer(srv, r, r.timer, r.requestDebugger, r.contextLogger)

	// adds personal middleware to handler.
	srv.MethodWith(proto.JSONRPCMethodWrapPersonal, r.personalWrapper)

	mux := http.NewServeMux()
	mux.Handle("/rpc", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
