package client_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/client"
	"gitlab.com/pjrpc/pjrpc/errs"
	"gitlab.com/pjrpc/pjrpc/model"
)

type testHTTPClient struct {
	req *http.Request

	resp *http.Response
	err  error
}

func (c *testHTTPClient) Do(req *http.Request) (*http.Response, error) {
	c.req = req
	return c.resp, c.err
}

func TestClientPositive(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	ctx := context.Background()
	id := "id_req"
	method := "method_name"
	param := "param string"
	resultType := ""
	result := &resultType

	cl, err := client.New("http://endpoint", client.ModWithBasicAuth("user", "pwd"))
	so.NoError(err)
	so.Equal("http://endpoint", cl.URL)
	so.Equal(1, len(cl.Mods))

	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":"res string"}`))

	tcl := &testHTTPClient{
		req: nil,
		resp: &http.Response{ // nolint:exhaustivestruct // This is testing!!!
			StatusCode:    http.StatusOK,
			Header:        http.Header{model.ContentTypeHeaderName: []string{model.ContentTypeHeaderValue}},
			Body:          ioutil.NopCloser(body),
			ContentLength: int64(body.Len()),
		},
		err: nil,
	}

	cl.HTTPClient = tcl
	err = cl.Invoke(ctx, id, method, param, result, client.ModWithHeader("custom", "value"))

	so.NoError(err)
	// Check mods
	user, pwd, ok := tcl.req.BasicAuth()
	so.Equal("user", user)
	so.Equal("pwd", pwd)
	so.True(ok)
	so.Equal("value", tcl.req.Header.Get("custom"))

	// Result was parsed correctly
	so.Equal("res string", *result)

	reqBody, err := ioutil.ReadAll(tcl.req.Body)
	so.NoError(err)
	so.Equal(
		`{"jsonrpc":"2.0","id":"id_req","method":"method_name","params":"param string"}`+"\n",
		string(reqBody),
	)

	//
	// error in response
	//
	body = bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","error":{"code":123}}`))
	tcl.resp.Body = ioutil.NopCloser(body)
	err = cl.Invoke(ctx, id, method, param, &result)
	jErr := &model.ErrorResponse{}
	so.True(errors.As(err, &jErr))
	so.Equal(123, jErr.Code)
}

var (
	errDoerErorr = errors.New("doer error")

	errCloseErrBody = errors.New("error body")
)

type errBody struct{}

func (*errBody) Read(_ []byte) (n int, err error) {
	return 0, nil
}

func (*errBody) Close() error {
	return errCloseErrBody
}

func TestClientNegative(t *testing.T) {
	t.Parallel()

	so := require.New(t)

	brokenURL := "http://end`point"
	validURL := "http://endpoint"
	ctx := context.Background()
	id := "id_req"
	method := "method_name"
	resultType := ""
	result := &resultType
	validParam := "valid_param"

	cl, err := client.New(brokenURL)
	urlErr := new(url.Error)
	so.True(errors.As(err, &urlErr))
	so.Equal("parse", urlErr.Op)
	so.Nil(cl)

	cl, err = client.New(validURL)
	so.NoError(err)
	so.NotNil(cl)

	//
	// can't marshall type
	//
	invalidParam := make(chan int)
	err = cl.Invoke(ctx, id, method, invalidParam, result)
	jsonErr := &json.UnsupportedTypeError{}
	so.True(errors.As(err, &jsonErr))

	//
	// can't create request
	//
	cl.URL = brokenURL
	err = cl.Invoke(ctx, id, method, validParam, result)
	so.True(errors.As(err, &urlErr))
	cl.URL = validURL

	//
	// common test client
	tcl := new(testHTTPClient)
	cl.HTTPClient = tcl

	//
	// error in http doer
	//
	tcl.err = errDoerErorr
	err = cl.Invoke(ctx, id, method, validParam, result)
	so.True(errors.Is(err, errDoerErorr))
	tcl.err = nil

	//
	// bad status code
	//
	tcl.resp = &http.Response{ // nolint:exhaustivestruct // This is testing!!!
		StatusCode: http.StatusBadRequest,
		Body:       ioutil.NopCloser(bytes.NewBuffer(nil)),
	}
	err = cl.Invoke(ctx, id, method, validParam, result)
	so.True(errors.Is(err, errs.ErrBadStatusCode))

	//
	// don't have valid content type
	//
	tcl.resp.StatusCode = http.StatusOK
	err = cl.Invoke(ctx, id, method, validParam, result)
	so.True(errors.Is(err, errs.ErrWrongContentType))

	//
	// invalid body
	//
	tcl.resp.Header = http.Header{model.ContentTypeHeaderName: []string{model.ContentTypeHeaderValue}}
	err = cl.Invoke(ctx, id, method, validParam, result)
	so.True(errors.Is(err, io.EOF))

	//
	// can't parse to invalid type
	//
	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":"res string"}`))
	tcl.resp.Body = ioutil.NopCloser(body)
	wrongResult := ""
	err = cl.Invoke(ctx, id, method, validParam, wrongResult)
	so.True(strings.Contains(err.Error(), "non-pointer"))

	//
	// Logging without logger
	//
	tcl.resp.StatusCode = http.StatusInternalServerError
	tcl.resp.Body = &errBody{}
	err = cl.Invoke(ctx, id, method, validParam, result)
	so.Error(err)

	log := new(bytes.Buffer)
	cl.SetLogger(log)

	err = cl.Invoke(ctx, id, method, validParam, result)
	so.Error(err)
	so.Equal("[pjrpc-client] (parseResponse): failed to close http response body: error body\n", log.String())
}
