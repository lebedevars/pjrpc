module helloworld

go 1.15

require (
	github.com/google/uuid v1.1.2
	gitlab.com/pjrpc/pjrpc v1.5.0
)
