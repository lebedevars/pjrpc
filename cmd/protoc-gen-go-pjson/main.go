package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/encoding/prototext"

	"gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjson/genpjson"
)

var (
	vesrion = "v1.1.0"

	flags            flag.FlagSet
	withoutOmitempty = flags.Bool(
		"without_omitempty",
		false,
		"generate json tags without omitempty",
	)

	pathProtoRequest = flags.String(
		"path_proto_request",
		"",
		"path to save protobuf request, will not save the file if the value is empty",
	)
)

func run(gen *protogen.Plugin) error {
	genpjson.Version = vesrion
	genpjson.WithoutOmitempty = *withoutOmitempty

	for _, f := range gen.Files {
		if !f.Generate {
			continue
		}

		genpjson.GenerateFile(gen, f)
	}

	if *pathProtoRequest == "" {
		return nil
	}

	file, err := os.Create(*pathProtoRequest)
	if err != nil {
		return fmt.Errorf("failed to create file for proto request: %w", err)
	}

	request, err := prototext.Marshal(gen.Request)
	if err != nil {
		return fmt.Errorf("prototext.Marshal: %w", err)
	}

	if _, err = file.Write(request); err != nil {
		return fmt.Errorf("file.Write: %w", err)
	}

	if err = file.Close(); err != nil {
		return fmt.Errorf("file.Close: %w", err)
	}

	return nil
}

func showVesrionsOrHelp() {
	if os.Args[1] == "--version" {
		fmt.Fprintf(os.Stderr, "%s %s\n", filepath.Base(os.Args[0]), vesrion)
		return
	}

	flags.PrintDefaults()
}

func main() {
	if len(os.Args) == 2 {
		showVesrionsOrHelp()
		os.Exit(0)
	}

	o := protogen.Options{
		ParamFunc:         flags.Set,
		ImportRewriteFunc: nil,
	}

	o.Run(run)
}
