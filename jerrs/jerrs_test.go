package jerrs_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/jerrs"
)

// TestNewServerError testing of the creation new JSON-RPC error.
func TestNewServerError(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	err := jerrs.NewServerError(100)
	so.Equal(100, err.Code)
	so.Equal("Server error", err.Message)
	so.Nil(err.Data)

	err = jerrs.NewServerError(101, nil)
	so.Equal(101, err.Code)
	so.Equal("Server error", err.Message)
	so.Equal(`null`, string(err.Data))

	err = jerrs.NewServerError(-32000, "Some text")
	so.Equal(-32000, err.Code)
	so.Equal("Server error", err.Message)
	so.Equal(`"Some text"`, string(err.Data))

	so.Panics(func() {
		err = jerrs.NewServerError(123, make(chan struct{}))
		so.Nil(err)
	})
}

// TestErrors testing error codes.
// https://www.jsonrpc.org/specification#error_object
func TestErrors(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	err := jerrs.ParseError()
	so.Equal(-32700, err.Code)
	so.Equal("Parse error", err.Message)
	so.Nil(err.Data)

	err = jerrs.InvalidRequest()
	so.Equal(-32600, err.Code)
	so.Equal("Invalid Request", err.Message)
	so.Nil(err.Data)

	err = jerrs.MethodNotFound()
	so.Equal(-32601, err.Code)
	so.Equal("Method not found", err.Message)
	so.Nil(err.Data)

	err = jerrs.InvalidParams()
	so.Equal(-32602, err.Code)
	so.Equal("Invalid params", err.Message)
	so.Nil(err.Data)

	err = jerrs.InternalError()
	so.Equal(-32603, err.Code)
	so.Equal("Internal error", err.Message)
	so.Nil(err.Data)

	err = jerrs.InvalidParams("some text")
	so.Equal(-32602, err.Code)
	so.Equal("Invalid params", err.Message)
	so.Equal(`"some text"`, string(err.Data))
}
