package router_test

import (
	"context"
	"encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/errs"
	"gitlab.com/pjrpc/pjrpc/server/router"
)

var errTest = errors.New("test error")

type ctxKey int

// TestRouterInvoke register handlers and invoke.
func TestRouterInvoke(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	const ctxKeyTest ctxKey = 1
	r := router.New()

	//
	// Just Invoke
	//

	methodName := "method_name"
	params := []byte("params")

	testContext := context.WithValue(context.Background(), ctxKeyTest, "keep_context_value")

	res, err := r.Invoke(testContext, methodName, params)
	so.Nil(res)
	so.True(errors.Is(err, errs.ErrRouteNotFound))

	handler := func(ctx context.Context, p json.RawMessage) (interface{}, error) {
		so.Equal(string(params), string(p))

		val, ok := ctx.Value(ctxKeyTest).(string)
		so.True(ok)
		so.Equal("keep_context_value", val)

		return 1, nil
	}

	r.RegisterMethod(methodName, handler)

	res, err = r.Invoke(testContext, methodName, params)
	so.NoError(err)
	so.Equal(res, 1)

	//
	// Invoke with error
	//

	methodName = "method_name_2"
	params = []byte("params_2")

	handlerErr := func(_ context.Context, p json.RawMessage) (interface{}, error) {
		so.Equal(string(params), string(p))

		return 2, errTest
	}

	res, err = r.Invoke(testContext, methodName, params)
	so.Nil(res)
	so.True(errors.Is(err, errs.ErrRouteNotFound))

	r.RegisterMethod(methodName, handlerErr)
	res, err = r.Invoke(context.Background(), methodName, params)
	so.Nil(res)
	so.True(errors.Is(err, errTest))

	//
	// Method already exists
	//
	so.Panics(func() { r.RegisterMethod(methodName, handlerErr) })
}

// TestRouterWith testing invoke with router middlewares.
func TestRouterWith(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	const (
		ctxKeyTestRouterMW       ctxKey = 10
		ctxKeyTestSecondRouterMW ctxKey = 11
	)
	r := router.New()

	//
	// Invoke with single middleware.
	//

	methodName := "method_with_router_wm"
	params := []byte("params")

	handler := func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		val, ok := ctx.Value(ctxKeyTestRouterMW).(string)
		so.True(ok)
		so.Equal(methodName, val)

		return 1, nil
	}

	middleware := func(next router.Handler) router.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			ctx = context.WithValue(ctx, ctxKeyTestRouterMW, methodName)
			return next(ctx, p)
		}
	}

	r.RegisterMethod(methodName, handler)
	r.With(middleware)

	res, err := r.Invoke(context.Background(), methodName, params)
	so.NoError(err)
	so.Equal(1, res)

	//
	// Invoke with two middleware in stack order.
	//
	methodName = "method_with_two_router_wm"

	handler = func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		val, ok := ctx.Value(ctxKeyTestRouterMW).(string)
		so.True(ok)
		so.Equal(methodName, val)

		val, ok = ctx.Value(ctxKeyTestSecondRouterMW).(string)
		so.True(ok)
		so.Equal("second_value", val)

		return 2, nil
	}

	middlewareTwo := func(next router.Handler) router.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			// registered middleware will overwrite this value
			ctx = context.WithValue(ctx, ctxKeyTestRouterMW, "it_will_be_overwrited")

			ctx = context.WithValue(ctx, ctxKeyTestSecondRouterMW, "second_value")

			return next(ctx, p)
		}
	}

	r.RegisterMethod(methodName, handler)
	r.With(middlewareTwo)

	res, err = r.Invoke(context.Background(), methodName, params)
	so.NoError(err)
	so.Equal(2, res)
}

// TestRouterMethodWith testing invoke with method middlewares.
func TestRouterMethodWith(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	r := router.New()
	const (
		ctxKeyTestMethodMW       ctxKey = 20
		ctxKeyTestSecondMethodMW ctxKey = 21
	)

	//
	// Invoke with single middleware.
	//

	methodName := "method_with_method_wm"
	params := []byte("params")

	handler := func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		val, ok := ctx.Value(ctxKeyTestMethodMW).(string)
		so.True(ok)
		so.Equal(methodName, val)

		return 1, nil
	}

	middleware := func(next router.Handler) router.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			ctx = context.WithValue(ctx, ctxKeyTestMethodMW, methodName)
			return next(ctx, p)
		}
	}

	so.Panics(func() { r.MethodWith(methodName, middleware) }) // try to set mw to method before register

	r.RegisterMethod(methodName, handler)
	r.MethodWith(methodName, middleware)

	res, err := r.Invoke(context.Background(), methodName, params)
	so.NoError(err)
	so.Equal(1, res)

	//
	// Invoke with two middleware in stack order.
	//
	methodName = "method_with_two_method_wm"

	handler = func(ctx context.Context, _ json.RawMessage) (interface{}, error) {
		val, ok := ctx.Value(ctxKeyTestMethodMW).(string)
		so.True(ok)
		so.Equal(methodName, val)

		val, ok = ctx.Value(ctxKeyTestSecondMethodMW).(string)
		so.True(ok)
		so.Equal("second_value", val)

		return 2, nil
	}

	middlewareTwo := func(next router.Handler) router.Handler {
		return func(ctx context.Context, p json.RawMessage) (interface{}, error) {
			// registered middleware will overwrite this value
			ctx = context.WithValue(ctx, ctxKeyTestMethodMW, "it_will_be_overwrited")

			ctx = context.WithValue(ctx, ctxKeyTestSecondMethodMW, "second_value")

			return next(ctx, p)
		}
	}

	r.RegisterMethod(methodName, handler)
	r.MethodWith(methodName, middleware)
	r.MethodWith(methodName, middlewareTwo)

	res, err = r.Invoke(context.Background(), methodName, params)
	so.NoError(err)
	so.Equal(2, res)
}
