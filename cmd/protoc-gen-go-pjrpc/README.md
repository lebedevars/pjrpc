protoc-gen-go-pjrpc
========

Golang JSON-RPC methods generator for [pjrpc](https://gitlab.com/pjrpc/pjrpc/) router.

# Install

```sh
go get -u gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjrpc
```

# Usage

```sh
protoc \
	--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
	protodir/file.proto
```

# Flags

`methods_with_service_name` - method name will be with service name prefix 'service_name_method_name' (false)

If you have multiple services and services contains methods with the same names then you have to use this flag.

```sh
--go-pjrpc_out=methods_with_service_name=true:. --go-pjrpc_opt=paths=source_relative \
```
