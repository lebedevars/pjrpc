package genpjrpc

import (
	"google.golang.org/protobuf/compiler/protogen"
)

func getClientIfaceMethod(g *protogen.GeneratedFile, m *MethodInfo) string {
	ctx := "ctx " + g.QualifiedGoIdent(contextPackage.Ident("Context"))
	in := "in *" + g.QualifiedGoIdent(m.Method.Input.GoIdent)
	out := "*" + g.QualifiedGoIdent(m.Method.Output.GoIdent)
	mods := "mods ..." + g.QualifiedGoIdent(clientPackage.Ident("Mod"))

	return m.Name + "(" + ctx + "," + in + "," + mods + ") (" + out + ", error)"
}

func genClientIfaceMethod(g *protogen.GeneratedFile, m *MethodInfo) {
	comment := checkAndGenCommentWithDeprecated(m.Method.Desc.Options(), m.Method.Comments.Leading)

	g.P(comment, getClientIfaceMethod(g, m))
}

func genClientImplMethod(g *protogen.GeneratedFile, clientImpl string, m *MethodInfo) {
	comment := checkAndGenCommentWithDeprecated(m.Method.Desc.Options(), m.Method.Comments.Leading)

	g.P(comment, "func (c *", clientImpl, ") ", getClientIfaceMethod(g, m), "{")
	g.P("gen, err := ", uuidPackage.Ident("NewUUID"), "()")
	g.P("if err != nil {")
	g.P("return nil,", fmtPackage.Ident("Errorf"), `("failed to create uuid generator: %w", err)`)
	g.P("}")
	g.P()
	g.P("result := new(", m.Method.Output.GoIdent, ")")
	g.P("err = c.cl.Invoke(ctx, gen.String(),", m.ConstName, ", in, &result, mods...)")
	g.P("if err != nil {")
	g.P("return nil,", fmtPackage.Ident("Errorf"), `("failed to Invoke method: %w", err)`)
	g.P("}")
	g.P()
	g.P("return result, nil")
	g.P("}")
	g.P()
}

func genClient(g *protogen.GeneratedFile, s *ServiceInfo) {
	clientName := s.Name + "Client"
	clientImpl := "impl" + clientName
	invkoer := g.QualifiedGoIdent(clientPackage.Ident("Invoker"))

	comment := " " + clientName + " is the client API for " + s.Name + " service."
	if s.Service.Comments.Leading != "" {
		comment += "\n" + string(s.Service.Comments.Leading)
	}

	g.P(
		checkAndGenCommentWithDeprecated(s.Service.Desc.Options(), protogen.Comments(comment)),
		"type ", clientName, " interface {",
	)
	for _, m := range s.Methods {
		genClientIfaceMethod(g, m)
	}
	g.P("}")
	g.P()

	g.P("type ", clientImpl, " struct {")
	g.P("cl ", invkoer)
	g.P("}")
	g.P()

	comment = " New" + clientName + " returns new client implementation " + s.Name + " API."
	g.P(
		checkAndGenCommentWithDeprecated(s.Service.Desc.Options(), protogen.Comments(comment)),
		"func New", clientName, "(cl ", invkoer, ")", clientName, "{",
	)
	g.P("return &", clientImpl, "{cl: cl}")
	g.P("}")
	g.P()

	for _, m := range s.Methods {
		genClientImplMethod(g, clientImpl, m)
	}
}
