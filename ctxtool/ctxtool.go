// Package ctxtool contains methods with context of the project.
package ctxtool

import (
	"context"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/model"
)

type ctxKey int

const (
	ctxKeyHTTPRequest ctxKey = iota + 1
	ctxKeyJSONRPCRequest
	ctxKeyIsBatch
)

// SetHTTPRequest sets *http.Request of the request to the context.
func SetHTTPRequest(ctx context.Context, r *http.Request) context.Context {
	return context.WithValue(ctx, ctxKeyHTTPRequest, r)
}

// GetHTTPRequest returns *http.Request of the request from the context.
func GetHTTPRequest(ctx context.Context) (r *http.Request, ok bool) {
	r, ok = ctx.Value(ctxKeyHTTPRequest).(*http.Request)
	return
}

// SetJSONRPCRequest sets JSON-RPC request to the context.
func SetJSONRPCRequest(ctx context.Context, r *model.Request) context.Context {
	return context.WithValue(ctx, ctxKeyJSONRPCRequest, r)
}

// GetJSONRPCRequest returns JSON-RPC request from the context.
func GetJSONRPCRequest(ctx context.Context) (r *model.Request, ok bool) {
	r, ok = ctx.Value(ctxKeyJSONRPCRequest).(*model.Request)
	return
}

// SetIsBatch sets a flag that indicates that this is a batch request or not.
func SetIsBatch(ctx context.Context, isBatch bool) context.Context {
	return context.WithValue(ctx, ctxKeyIsBatch, isBatch)
}

// GetIsBatch returns a flag about batch request.
func GetIsBatch(ctx context.Context) (isBatch, ok bool) {
	isBatch, ok = ctx.Value(ctxKeyIsBatch).(bool)
	return
}
