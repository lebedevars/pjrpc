// Package errs contains golang errors of the projects.
package errs

import (
	"errors"
)

var (
	// ErrRouteAlreadyExists returns in registration routeres in storage.
	ErrRouteAlreadyExists = errors.New("route already exists")

	// ErrRouteNotFound returns when storage can't found handler by route.
	ErrRouteNotFound = errors.New("route not found")

	// ErrPanicInHandler returns in the server when handler called panic.
	ErrPanicInHandler = errors.New("panic in handler")

	// ErrBadStatusCode returns in client when http response has code not 200.
	ErrBadStatusCode = errors.New("bad status code")

	// ErrWrongContentType returns in client when http response has content-type not application/json.
	ErrWrongContentType = errors.New("wrong content type")
)
