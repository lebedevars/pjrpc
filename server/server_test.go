package server_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/errs"
	"gitlab.com/pjrpc/pjrpc/jerrs"
	"gitlab.com/pjrpc/pjrpc/model"
	"gitlab.com/pjrpc/pjrpc/server"
)

var errCustomError = errors.New("custom error")

type badReaderCloser struct{}

func (*badReaderCloser) Read(_ []byte) (n int, err error) {
	return 0, errCustomError
}

func (*badReaderCloser) Close() error {
	return errCustomError
}

type badResponseWriter struct{}

func (*badResponseWriter) Header() http.Header { return make(http.Header) }

func (*badResponseWriter) Write(_ []byte) (int, error) { return 0, errCustomError }

func (*badResponseWriter) WriteHeader(_ int) {}

func createRequest(t *testing.T, body string) (*httptest.ResponseRecorder, *http.Request) {
	t.Helper()

	req, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodPost,
		"/rpc",
		strings.NewReader(body),
	)
	if err != nil {
		t.Fatalf("failed to create http request")
	}

	req.Header.Set("Content-Type", "application/json;charset=UTF-8")

	rr := httptest.NewRecorder()

	return rr, req
}

func checkProtocol(so *require.Assertions, rr *httptest.ResponseRecorder, id string) *model.Response {
	so.Equal(http.StatusOK, rr.Code)

	resp := new(model.Response)
	err := json.NewDecoder(rr.Body).Decode(resp)
	so.NoError(err, "decode response body")

	so.Equal(rr.Header().Get("Content-Type"), "application/json")

	so.Equal("2.0", resp.JSONRPC, "jsonrpc protocol")
	so.Equal(id, resp.ID, "id request")

	rr.Flush()

	return resp
}

func checkError(so *require.Assertions, expErr, actualErr *model.ErrorResponse) {
	if expErr == nil {
		so.Nil(actualErr)
		return
	}

	so.NotNil(actualErr)
	so.Equal(expErr.Code, actualErr.Code, "error code")
	so.Equal(expErr.Message, actualErr.Message, "error message")

	if expErr.Data == nil {
		so.Nil(actualErr.Data)
		return
	}

	so.Equal(expErr.Data, actualErr.Data, "error data")
}

func TestServer(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	srv := server.New()

	//
	// parseRequest
	//

	// request without header == invalid request
	rr, req := createRequest(t, "")
	req.Header.Del("Content-Type")
	srv.ServeHTTP(rr, req)
	resp := checkProtocol(so, rr, "")
	so.Nil(resp.Result)
	checkError(so, jerrs.InvalidRequest("content-type must be 'application/json'"), resp.Error)

	// request without body == parse error
	rr, req = createRequest(t, "")
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "")
	so.Nil(resp.Result)
	checkError(so, jerrs.InvalidRequest("body length is 0"), resp.Error)

	// request with wrong protocol == invalid request
	rr, req = createRequest(t, `{"jsonrpc":"1.0"}`)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "")
	so.Nil(resp.Result)
	checkError(so, jerrs.InvalidRequest("wrong value of the field 'jsonrpc'"), resp.Error)

	// invalid body == invalid request
	rr, req = createRequest(t, `{[]}`)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "")
	so.Nil(resp.Result)
	checkError(so, jerrs.InvalidRequest("failed to parse request"), resp.Error)

	// read from closed body
	rr, req = createRequest(t, `{"jsonrpc":"2.0"}`)
	req.Body = new(badReaderCloser)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "")
	so.Nil(resp.Result)
	checkError(so, jerrs.ParseError("failed to read body"), resp.Error)

	//
	// Method not found
	//
	// request without/wrong method name == method not found
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"1","method":"method","params":{}}`)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "1")
	so.Nil(resp.Result)
	checkError(so, jerrs.MethodNotFound(), resp.Error)

	//
	// Method with custom error
	//
	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		so.Equal(`{"field":"value"}`, string(params))
		return nil, errCustomError
	})
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"2","method":"method","params":{"field":"value"}}`)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "2")
	so.Nil(resp.Result)
	checkError(so, jerrs.NewServerError(-32000, `method 'method': custom error`), resp.Error)

	//
	// Method with invalid type
	//
	srv.RegisterMethod("invalid_type", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		return make(chan int), nil
	})
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"2.1","method":"invalid_type","params":{}}`)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "2.1")
	so.Nil(resp.Result)
	checkError(so, jerrs.InternalError("failed to marshal response"), resp.Error)

	//
	// Method without error
	//
	srv.RegisterMethod("method_2", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		so.Equal(`{"field2":"value2"}`, string(params))
		return "just text result", nil
	})
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"3","method":"method_2","params":{"field2":"value2"}}`)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "3")
	checkError(so, nil, resp.Error)
	so.Equal(`"just text result"`, string(resp.Result))

	//
	// With invalid content-length
	//
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"3.1","method":"method_2","params":{"field2":"value2"}}`)
	req.ContentLength = -1
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "3.1")
	checkError(so, nil, resp.Error)
	so.Equal(`"just text result"`, string(resp.Result))

	//
	// Check panic in handler
	//
	methodWithPanicBody := `{"jsonrpc":"2.0","id":"4","method":"method_with_panic","params":{}}`
	srv.RegisterMethod("method_with_panic", func(_ context.Context, _ json.RawMessage) (interface{}, error) {
		panic("test panic")
		return "method_with_panic result", nil // nolint:govet // 'unreachable: unreachable code', Yes it is
	})

	// With default panic handler
	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "4")
	checkError(so, jerrs.InternalError(), resp.Error)

	// With custom panic handler, that just set crazy status code.
	srv.OnPanic = func(resp *model.Response, err error) {
		so.True(errors.Is(err, errs.ErrPanicInHandler))
		resp.Error = jerrs.InvalidRequest()
	}

	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)
	resp = checkProtocol(so, rr, "4")
	checkError(so, jerrs.InvalidRequest(), resp.Error)

	// With Logger without panic handler
	srv.OnPanic = nil
	log := bytes.NewBuffer(nil)
	srv.SetLogger(log)
	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)
	so.Equal(
		"[pjrpc-server] (restorePanic): panic: panic in handler: 'method_with_panic': test panic\n",
		log.String(),
	)

	//
	// Without panic handlers just print in log
	//
	srv.Logger = nil
	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)

	//
	// log error in writing response body
	//
	log = bytes.NewBuffer(nil)
	srv.SetLogger(log)
	_, req = createRequest(t, `{"jsonrpc":"2.0","id":"5","method":"method_2","params":{"field2":"value2"}}`)
	srv.ServeHTTP(new(badResponseWriter), req)
	so.Equal("[pjrpc-server] (sendResponseBody): failed to write response: custom error\n", log.String())
}

func checkProtocolBatch(as *require.Assertions, rr *httptest.ResponseRecorder, ids ...string) model.BatchResponses {
	as.Equal(http.StatusOK, rr.Code)

	var respBatch model.BatchResponses

	body := rr.Body.String()
	err := json.Unmarshal([]byte(body), &respBatch)
	as.NoError(err, "decode response body: '%s'", body)

	as.Equal(rr.Header().Get("Content-Type"), "application/json")

	for i, resp := range respBatch {
		as.Equal("2.0", resp.JSONRPC, "jsonrpc protocol")
		as.Equal(ids[i], resp.ID, "id request")
	}

	rr.Flush()

	return respBatch
}

func TestServerBatch(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	srv := server.New()

	//
	// Single request in Batch
	//
	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		if string(params) == `{"single":"request"}` {
			return "result", nil
		}
		return "multi", nil
	})

	rr, req := createRequest(t, `[{"jsonrpc":"2.0","id":"1","method":"method","params":{"single":"request"}}]`)
	srv.ServeHTTP(rr, req)
	respBatch := checkProtocolBatch(so, rr, "1")
	so.Equal(1, len(respBatch))

	checkError(so, nil, respBatch[0].Error)
	so.Equal(`"result"`, string(respBatch[0].Result))

	//
	// Many requests in Batch
	//
	rr, req = createRequest(t, `[
		{"jsonrpc":"2.0","id":"1","method":"method","params":{"single":"request"}},
		{"jsonrpc":"2.0","id":"2","method":"method","params":{"single":"multi"}}
	]`)
	srv.ServeHTTP(rr, req)
	respBatch = checkProtocolBatch(so, rr, "1", "2")
	so.Equal(2, len(respBatch))

	checkError(so, nil, respBatch[0].Error)
	so.Equal(`"result"`, string(respBatch[0].Result))
	checkError(so, nil, respBatch[1].Error)
	so.Equal(`"multi"`, string(respBatch[1].Result))

	//
	// Many requests with error Batch
	//
	srv.RegisterMethod("error", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		return nil, jerrs.InternalError("error")
	})
	rr, req = createRequest(t, `[
		{"jsonrpc":"2.0","id":"1","method":"method","params":{"single":"request"}},
		{"jsonrpc":"2.0","id":"2","method":"error","params":{"single":"multi"}}
	]`)
	srv.ServeHTTP(rr, req)
	respBatch = checkProtocolBatch(so, rr, "1", "2")
	so.Equal(2, len(respBatch))

	checkError(so, nil, respBatch[0].Error)
	so.Equal(`"result"`, string(respBatch[0].Result))
	checkError(so, jerrs.InternalError("error"), respBatch[1].Error)
	so.Nil(respBatch[1].Result)

	//
	// Batch request with wrong body
	//
	rr, req = createRequest(t, `[[]]`)
	srv.ServeHTTP(rr, req)
	respBatch = checkProtocolBatch(so, rr, "")
	so.Equal(1, len(respBatch))
	checkError(so, jerrs.InvalidRequest("failed to parse request"), respBatch[0].Error)
	so.Nil(respBatch[0].Result)

	//
	// Many requests with invalid request Batch
	//
	rr, req = createRequest(t, `[{"jsonrpc":"1.0","id":"3"}]`)
	srv.ServeHTTP(rr, req)
	respBatch = checkProtocolBatch(so, rr, "3")
	so.Equal(1, len(respBatch))

	checkError(so, jerrs.InvalidRequest("wrong value of the field 'jsonrpc'"), respBatch[0].Error)
	so.Nil(respBatch[0].Result)
}
