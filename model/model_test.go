package model_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/model"
)

type dataType struct {
	Data string
}

func (dt dataType) MarshalJSON() ([]byte, error) {
	return []byte(`"` + dt.Data + `"`), nil
}

// TestErrorString testing of the error string formatter.
func TestErrorString(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	e := &model.ErrorResponse{
		Code:    1234,
		Message: "Custom Error",
		Data:    nil,
	}

	so.Equal("JSON-RPC Error: [1234] Custom Error", e.Error())

	e.Data = json.RawMessage(`"Some text"`)
	so.Equal(`JSON-RPC Error: [1234] Custom Error ("Some text")`, e.Error())

	structData := struct {
		Data string
	}{
		Data: "Some text",
	}

	var err error
	e.Data, err = json.Marshal(structData)
	so.NoError(err)

	so.Equal(`JSON-RPC Error: [1234] Custom Error ({"Data":"Some text"})`, e.Error())

	e.Data, err = json.Marshal(dataType{Data: "Some text"})
	so.NoError(err)

	so.Equal(`JSON-RPC Error: [1234] Custom Error ("Some text")`, e.Error())

	e = nil
	so.Equal("<nil>", e.Error())
}
