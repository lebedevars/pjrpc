package genpjrpc

import (
	"google.golang.org/protobuf/compiler/protogen"
)

func genServerIfaceMethod(g *protogen.GeneratedFile, m *MethodInfo) {
	ctx := "ctx " + g.QualifiedGoIdent(contextPackage.Ident("Context"))
	in := "in *" + g.QualifiedGoIdent(m.Method.Input.GoIdent)
	out := "*" + g.QualifiedGoIdent(m.Method.Output.GoIdent)

	g.P(
		checkAndGenCommentWithDeprecated(m.Method.Desc.Options(), m.Method.Comments.Leading),
		m.Name, "(", ctx, ",", in, ") (", out, ", error)",
	)
}

func genRegistredHandler(g *protogen.GeneratedFile, registrator string, m *MethodInfo) {
	ctx := "ctx " + g.QualifiedGoIdent(contextPackage.Ident("Context"))
	params := "params " + g.QualifiedGoIdent(jsonPackage.Ident("RawMessage"))

	g.P("func (r *", registrator, ") ", m.RegHandlerName, "(", ctx, ",", params, ") (interface{}, error) {")
	g.P("in := new(", m.Method.Input.GoIdent, ")")
	g.P("if params != nil {")
	g.P("err := ", jsonPackage.Ident("Unmarshal"), "(params, in)")
	g.P("if err != nil {")
	g.P("return nil, ", jerrsPackage.Ident("ParseError"), `("failed to parse params")`)
	g.P("}")
	g.P("}")
	g.P()
	g.P("res, err := r.srv.", m.Method.GoName, "(ctx, in)")
	g.P("if err != nil {")
	g.P("return nil, ", fmtPackage.Ident("Errorf"), `("failed `, m.Method.GoName, `: %w", err)`)
	g.P("}")
	g.P()
	g.P("return res, nil")
	g.P("}")
	g.P()
}

func genServer(g *protogen.GeneratedFile, s *ServiceInfo) {
	serverName := s.Name + "Server"
	registratorName := "reg" + s.Name

	comment := " " + serverName + " is the server API for " + s.Name + " service."
	if s.Service.Comments.Leading != "" {
		comment += "\n" + string(s.Service.Comments.Leading)
	}

	g.P(
		checkAndGenCommentWithDeprecated(s.Service.Desc.Options(), protogen.Comments(comment)),
		"type ", serverName, " interface {",
	)
	for _, m := range s.Methods {
		genServerIfaceMethod(g, m)
	}
	g.P("}")
	g.P()

	g.P("type ", registratorName, " struct {")
	g.P("srv ", serverName)
	g.P("}")
	g.P()

	server := "s *" + g.QualifiedGoIdent(serverPackage.Ident("Server"))
	mws := "middlewares ..." + g.QualifiedGoIdent(routerPackage.Ident("Middleware"))

	comment = " Register" + serverName + " registers rpc handlers with middlewares in the server router."
	g.P(
		checkAndGenCommentWithDeprecated(s.Service.Desc.Options(), protogen.Comments(comment)),
		"func Register", serverName, "(", server, ", ", "srv ", serverName, ",", mws, ") {",
	)
	g.P("r := &", registratorName, "{srv: srv}")
	g.P()
	for _, m := range s.Methods {
		g.P("s.RegisterMethod(", m.ConstName, ", r.", m.RegHandlerName, ")")
	}
	g.P()
	g.P("for _, mw := range middlewares {")
	g.P("s.With(mw)")
	g.P("}")
	g.P("}")
	g.P()

	for _, m := range s.Methods {
		genRegistredHandler(g, registratorName, m)
	}
}
