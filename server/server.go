// Package server contains http handler "Server" with JSON-RPC router.
package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"gitlab.com/pjrpc/pjrpc/ctxtool"
	"gitlab.com/pjrpc/pjrpc/errs"
	"gitlab.com/pjrpc/pjrpc/jerrs"
	"gitlab.com/pjrpc/pjrpc/model"
	"gitlab.com/pjrpc/pjrpc/server/router"
)

// Server http handler with JSON-RPC router.
type Server struct {
	*router.Router

	// Logger is a optional logger that writes errors in write response method.
	// Also writes panics in handlers.
	Logger *log.Logger

	// Panic handler calls when your rpc handler make panic.
	// There is default panic handler (DefaultRestoreOnPanic) after created by New method.
	OnPanic func(resp *model.Response, err error)

	// OnFailedParseRequest handler calls when server can't parse client request.
	// There is default handler (DefaultOnFailedParseRequest) after created by New method.
	OnFailedParseRequest func(r *http.Request, err error) *model.ErrorResponse
}

// serveResult represents http request with parsed body.
// It must contain at least one request.
// Each request has response empty or with parse error.
type serveResult struct {
	requests  model.BatchRequests
	responses model.BatchResponses
	isBatch   bool
}

// New creates new server with default panic handler and empty router and logger.
func New() *Server {
	s := &Server{
		Router: router.New(),
		Logger: nil,

		OnPanic:              nil,
		OnFailedParseRequest: nil,
	}

	s.OnPanic = s.DefaultRestoreOnPanic
	s.OnFailedParseRequest = s.DefaultOnFailedParseRequest

	return s
}

// DefaultRestoreOnPanic is a default panic handler that converts panic in JSON-RPC protocol error.
// It prints error to log if it set and puts normal response body with JSON-RPC error.
func (s *Server) DefaultRestoreOnPanic(resp *model.Response, err error) {
	s.logError("DefaultRestoreOnPanic", "panic", err)
	setError(resp, jerrs.InternalError())
}

// DefaultOnFailedParseRequest default handler calls when server can't to parse client request.
// Returns JSON-RPC error with static text.
func (s *Server) DefaultOnFailedParseRequest(r *http.Request, err error) *model.ErrorResponse {
	return jerrs.InvalidRequest("failed to parse request")
}

// SetLogger sets your io.Writer as error logger of the pjrpc server.
// Also you can create and set your own *log.Logger in Logger field.
func (s *Server) SetLogger(w io.Writer) {
	s.Logger = log.New(w, "[pjrpc-server] ", 0)
}

func (s *Server) logError(place, message string, err error) {
	if s.Logger == nil {
		return
	}

	s.Logger.Printf("(%s): %s: %s\n", place, message, err)
}

func convertError(err error) *model.ErrorResponse {
	jrpcErr := &model.ErrorResponse{}
	if errors.As(err, &jrpcErr) {
		return jrpcErr
	}

	if errors.Is(err, errs.ErrRouteNotFound) {
		return jerrs.MethodNotFound()
	}

	return jerrs.NewServerError(-32000, err.Error())
}

func setError(resp *model.Response, err error) {
	resp.Error = convertError(err)
	resp.Result = nil
}

func setResult(resp *model.Response, result interface{}) {
	resultJSON, err := json.Marshal(result)
	if err != nil {
		setError(resp, jerrs.InternalError("failed to marshal response"))
		return
	}

	resp.Result = resultJSON
	resp.Error = nil
}

func isRequestValid(req *model.Request) error {
	if req.JSONRPC != model.JSONRPCVersion {
		return jerrs.InvalidRequest("wrong value of the field 'jsonrpc'")
	}

	return nil
}

func createResponse(req *model.Request) *model.Response {
	return &model.Response{
		JSONRPC: model.JSONRPCVersion,
		ID:      req.ID,
		Result:  nil,
		Error:   nil,
	}
}

func parseSingleRequest(res *serveResult, decoder *json.Decoder) error {
	r := new(model.Request)
	err := decoder.Decode(&r)
	if err != nil {
		return fmt.Errorf("decoder.Decode: %w", err)
	}

	resp := createResponse(r)
	if err = isRequestValid(r); err != nil {
		setError(resp, err)
	}

	res.requests = model.BatchRequests{r}
	res.responses = model.BatchResponses{resp}

	return nil
}

func parseBatchRequest(res *serveResult, decoder *json.Decoder) error {
	err := decoder.Decode(&res.requests)
	if err != nil {
		return fmt.Errorf("decoder.Decode: %w", err)
	}

	res.responses = make(model.BatchResponses, len(res.requests))

	for i, r := range res.requests {
		res.responses[i] = createResponse(r)

		if err = isRequestValid(r); err != nil {
			setError(res.responses[i], err)
		}
	}

	return nil
}

func (s *Server) parseRequest(r *http.Request) (*serveResult, error) {
	defer func() {
		if err := r.Body.Close(); err != nil {
			s.logError("parseRequest", "failed to close request body", err)
		}
	}()

	if !strings.Contains(r.Header.Get(model.ContentTypeHeaderName), model.ContentTypeHeaderValue) {
		return nil, jerrs.InvalidRequest("content-type must be 'application/json'")
	}

	contentLength := r.ContentLength
	if contentLength < 0 {
		contentLength = 0
	}

	body := bytes.NewBuffer(make([]byte, 0, contentLength))

	_, err := body.ReadFrom(r.Body)
	if err != nil {
		return nil, jerrs.ParseError("failed to read body")
	}

	if body.Len() == 0 {
		return nil, jerrs.InvalidRequest("body length is 0")
	}

	firstSymbol, _, err := body.ReadRune()
	if err != nil {
		s.logError("parseRequest", "failed to body.ReadRune", err)
		return nil, jerrs.InternalError()
	}
	if err = body.UnreadRune(); err != nil {
		s.logError("parseRequest", "failed to body.UnreadRune", err)
		return nil, jerrs.InternalError()
	}

	res := &serveResult{
		requests:  nil,
		responses: nil,
		isBatch:   firstSymbol == '[',
	}

	decoder := json.NewDecoder(body)

	if res.isBatch {
		err = parseBatchRequest(res, decoder)
	} else {
		err = parseSingleRequest(res, decoder)
	}

	if err != nil {
		return res, s.OnFailedParseRequest(r, err)
	}

	return res, nil
}

func (s *Server) sendResponseBody(w http.ResponseWriter, body json.RawMessage) {
	w.Header().Set(model.ContentTypeHeaderName, model.ContentTypeHeaderValue)
	w.WriteHeader(http.StatusOK)

	if _, err := w.Write(body); err != nil {
		s.logError("sendResponseBody", "failed to write response", err)
		return
	}
}

func (s *Server) sendError(w http.ResponseWriter, res *serveResult, err error) {
	var srcBody interface{}

	resp := &model.Response{JSONRPC: model.JSONRPCVersion} // nolint:exhaustivestruct // no need all fields
	setError(resp, err)

	if res != nil && res.isBatch {
		srcBody = model.BatchResponses{resp}
	} else {
		srcBody = resp
	}

	body, err := json.Marshal(srcBody)
	if err != nil {
		s.logError("sendError", "json.Marshal model.Response", err) // unreal error
		return
	}

	s.sendResponseBody(w, body)
}

func (s *Server) sendServeResult(w http.ResponseWriter, res *serveResult) {
	var body []byte
	var err error

	if res.isBatch {
		body, err = json.Marshal(res.responses)
	} else {
		body, err = json.Marshal(res.responses[0])
	}

	if err != nil {
		s.logError("sendServeResult", "json.Marshal serveResult", err) // unreal error
		return
	}

	s.sendResponseBody(w, body)
}

func (s *Server) restoreOnPanic(resp *model.Response, r interface{}, method string) {
	err := fmt.Errorf("%w: '%s': %v", errs.ErrPanicInHandler, method, r)

	if s.OnPanic != nil {
		s.OnPanic(resp, err)
	} else {
		s.logError("restorePanic", "panic", err)
	}

	if s.OnPanic == nil && s.Logger == nil {
		println(err.Error())
	}
}

func (s *Server) invokeMethod(res *serveResult, index int, r *http.Request) {
	if res.responses[index].Error != nil { // Error could have been set in during parsing.
		return
	}

	jReq := res.requests[index]

	ctx := r.Context() // this keeps the ability to add custom information to context in batch requests.
	ctx = ctxtool.SetHTTPRequest(ctx, r)
	ctx = ctxtool.SetJSONRPCRequest(ctx, jReq)
	ctx = ctxtool.SetIsBatch(ctx, res.isBatch)

	defer func() {
		if rec := recover(); rec != nil {
			s.restoreOnPanic(res.responses[index], rec, jReq.Method)
		}
	}()

	result, err := s.Router.Invoke(ctx, jReq.Method, jReq.Params)
	if err != nil {
		setError(res.responses[index], err)
	} else {
		setResult(res.responses[index], result)
	}
}

// ServeHTTP implements interface of handler in http package.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	res, err := s.parseRequest(r)
	if err != nil {
		s.sendError(w, res, err)
		return
	}

	for i := 0; i < len(res.requests); i++ {
		s.invokeMethod(res, i, r)
	}

	s.sendServeResult(w, res)
}
