pjrpc - Proto-JSON-RPC
========

Golang JSON-RPC router with Protobuf (Swagger included).

# Features

- **Lightweight** - simple and fast router
- **JSON-RPC protocol** - support protocol [v2.0](https://www.jsonrpc.org/specification) + Batch requests
- **Go std-lib compatible** - net/http, json.RawMessage
- **Go way context usage** - ctx in your handlers with data
- **Handler middlewares** - you can set common or personal method middlewares
- **Boilerplate code generator** - unmarshalling JSON params to model
- **Protobuf spec** - describe your types and methods with protobuf specs
- **Swagger spec** - generate [swagger](https://gitlab.com/pjrpc/proto-swagger-pjrpc) documentation using protobuf spec
- **WASM Client** - JSON-RPC client with proto files can compile to [WASM](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/wasm)
- **Using as JSON-RPC router** - you can use this router like a simple JSON-RPC router [without code-gen](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/without-code-gen)

[Benchmarks](https://gitlab.com/pjrpc/benchmarks)

# Installation and use

- `go-pjson_out` is a type generator plugin
- `go-pjrpc_out` is a json-rpc methods generator plugin

```sh
export GO111MODULE=on
go get -u gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjrpc
go get -u gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjson

protoc \
	--go-pjson_out=. --go-pjson_opt=paths=source_relative \
	--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
	protodir/file.proto
```

Also you can use google golang type generator or whatever with json tags.

```sh
go get -u google.golang.org/protobuf/cmd/protoc-gen-go

protoc \
	--go_out=. --go_opt=paths=source_relative \
	--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
	protodir/file.proto
```

## Generators flags

### protoc-gen-go-pjson:

`without_omitempty` - generate json tags without omitempty

```sh
--go-pjson_out=without_omitempty=true:. --go-pjson_opt=paths=source_relative \
```

### protoc-gen-go-pjrpc:

`methods_with_service_name` - method name will be with service name prefix 'service_name_method_name' (false)

If you have multiple services and services contains methods with the same names then you have to use this flag.

```sh
--go-pjrpc_out=methods_with_service_name=true:. --go-pjrpc_opt=paths=source_relative \
```

# Examples

- [pkg.go.dev](https://pkg.go.dev/gitlab.com/pjrpc/pjrpc)
- [Hello world](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/helloworld)
- [Middlewares + Client mods](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/middlewares)
- [WASM](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/wasm)
- [Without code gen](https://gitlab.com/pjrpc/pjrpc/-/tree/master/_examples/without-code-gen)


## Server example

```go
package main

import (
	"context"
	"errors"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/jerrs"
	"gitlab.com/pjrpc/pjrpc/server"

	"helloworld/proto"
)

var errServer = errors.New("server error")

type rpc struct{}

// Hello responder.
func (r *rpc) Hello(ctx context.Context, in *proto.HelloRequest) (*proto.HelloResponse, error) {
	log.Printf("got message from: %s", in.Name)

	switch in.Name {
	case "rpc client": // it's ok, pass
	case "error": // return golang error
		return nil, errServer
	case "panic":
		panic("rpc client sent panic")
	default: // returns json-rpc error
		return nil, jerrs.InvalidParams("wrong name")
	}

	return &proto.HelloResponse{Name: "rpc server"}, nil
}

func main() {
	srv := server.New()
	srv.SetLogger(log.Writer())

	r := &rpc{}

	proto.RegisterHelloWorlderServer(srv, r)

	mux := http.NewServeMux()
	mux.Handle("/rpc", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
```

**request:**

```json
{
  "jsonrpc": "2.0",
  "id": "1",
  "method": "hello",
  "params": {
    "name": "rpc client"
  }
}
```

**response:**
```json
{
  "jsonrpc": "2.0",
  "id": "1",
  "result": {
    "name": "rpc server"
  }
}
```
