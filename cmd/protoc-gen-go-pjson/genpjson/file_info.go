package genpjson

import (
	"google.golang.org/protobuf/compiler/protogen"
)

// EnumInfo proto enum.
type EnumInfo struct {
	*protogen.Enum
}

// FieldInfo proto field of message with canon JSON name.
type FieldInfo struct {
	*protogen.Field

	JSONName string
}

// MessageInfo proto message with fields list.
type MessageInfo struct {
	*protogen.Message

	FieldsInfo []*FieldInfo
}

// FileInfo proto file with all enums and messages in "flattened ordering".
type FileInfo struct {
	*protogen.File

	Enums    []*EnumInfo
	Messages []*MessageInfo
}

// GetFileInfo returns file info with all enums and messages.
func GetFileInfo(file *protogen.File) *FileInfo {
	f := &FileInfo{File: file}

	// Collect all enums, messages, and extensions in "flattened ordering".
	// See filetype.TypeBuilder.
	var walkMessages func([]*protogen.Message, func(*protogen.Message))

	walkMessages = func(messages []*protogen.Message, fn func(*protogen.Message)) {
		for _, m := range messages {
			fn(m)
			walkMessages(m.Messages, fn)
		}
	}

	initEnumInfos := func(enums []*protogen.Enum) {
		for _, enum := range enums {
			f.Enums = append(f.Enums, &EnumInfo{
				Enum: enum,
			})
		}
	}

	initMessageInfos := func(messages []*protogen.Message) {
		for _, message := range messages {
			fields := make([]*FieldInfo, len(message.Fields))

			for i, f := range message.Fields {
				fields[i] = &FieldInfo{
					Field:    f,
					JSONName: string(f.Desc.Name()),
				}
			}

			f.Messages = append(f.Messages, &MessageInfo{
				Message:    message,
				FieldsInfo: fields,
			})
		}
	}

	initEnumInfos(file.Enums)
	initMessageInfos(file.Messages)

	walkMessages(file.Messages, func(m *protogen.Message) {
		initEnumInfos(m.Enums)
		initMessageInfos(m.Messages)
	})

	return f
}
