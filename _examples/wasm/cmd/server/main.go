package main

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/server"

	"wasm/proto"
)

type rpc struct{}

func (r *rpc) Procedure(ctx context.Context, in *proto.Request) (*proto.Response, error) {
	log.Println("got request: ", in.Field)
	return &proto.Response{FieldOut: "out field"}, nil
}

func main() {
	srv := server.New()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.

	r := &rpc{}

	proto.RegisterWebAssemblyRPCServer(srv, r)

	mux := http.NewServeMux()

	fs := http.FileServer(http.Dir("./static"))

	mux.Handle("/static/", http.StripPrefix("/static", fs))
	mux.Handle("/rpc", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
