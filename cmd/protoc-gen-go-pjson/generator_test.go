package main

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/compiler/protogen"
	"google.golang.org/protobuf/encoding/prototext"
	"google.golang.org/protobuf/types/pluginpb"
)

func testingGenerator(t *testing.T, dumpFile, expectedProto string) {
	t.Log(expectedProto)
	t.Log(dumpFile)

	expectedProtoFile, err := ioutil.ReadFile(expectedProto)
	if err != nil {
		t.Fatal("ioutil.ReadFile expected proto file: ", err)
	}

	requestDump, err := ioutil.ReadFile(dumpFile)
	if err != nil {
		t.Fatal("ioutil.ReadFile dump file: ", err)
	}

	request := new(pluginpb.CodeGeneratorRequest)
	err = prototext.Unmarshal(requestDump, request)
	if err != nil {
		t.Fatal("prototext.Unmarshal: ", err)
	}

	o := protogen.Options{
		ParamFunc:         flags.Set,
		ImportRewriteFunc: nil,
	}

	plugin, err := o.New(request)
	if err != nil {
		t.Fatal("protogen.Options.New: ", err)
	}

	err = run(plugin)
	if err != nil {
		t.Fatal("run: ", err)
	}

	so := require.New(t)
	resp := plugin.Response()

	so.Equal(1, len(resp.File))

	so.Equal(string(expectedProtoFile), resp.File[0].GetContent())
}

func TestGenerator(t *testing.T) {
	testingGenerator(
		t,
		"testdata/imppkg/proto_request.txt",
		"testdata/imppkg/imppkg_pjson.pb.go",
	)

	testingGenerator(
		t,
		"testdata/proto_request.txt",
		"testdata/test_proto_pjson.pb.go",
	)
}
