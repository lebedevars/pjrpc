package main

import (
	"context"
	"log"
	"net/http"
	"net/http/httputil"

	"gitlab.com/pjrpc/pjrpc/client"

	"middlewares/proto"
)

// registered in client.New and will be called before each request.
func modRequestDebbuger(req *http.Request) {
	dump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		log.Println("failed to DumpRequestOut:", err)
	}

	log.Println("request: ", string(dump))
}

func main() {
	cl, err := client.New("http://127.0.0.1:8080/rpc", modRequestDebbuger)
	if err != nil {
		log.Fatalf("failed to client.New: %s", err)
	}
	cl.SetLogger(log.Writer()) // Client can write errors about closing the response body.

	rpc := proto.NewMiddlewareTesterClient(cl)

	respJM, err := rpc.JustMethod(context.Background(), &proto.HelloRequest{Name: "without personal mods"})
	if err != nil {
		log.Fatalln("rpc.JustMethod:", err)
	}

	log.Println("response JustMethod:", respJM)

	req := &proto.HelloRequest{Name: "with personal mod"}
	respWP, err := rpc.WrapPersonal(context.Background(), req, client.ModWithHeader("X-Header-Client", "Value"))
	if err != nil {
		log.Fatalln("rpc.WrapPersonal:", err)
	}

	log.Println("response WrapPersonal:", respWP)
}
