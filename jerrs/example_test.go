package jerrs_test

import (
	"encoding/json"
	"fmt"

	"gitlab.com/pjrpc/pjrpc/jerrs"
)

type customErrorData struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func Example() {
	// Specification errors with/without your data.
	jerr := jerrs.InternalError()
	fmt.Println(jerr)

	jerr = jerrs.InternalError("some text")
	fmt.Println(jerr)

	jerr = jerrs.InternalError(nil)
	fmt.Println(jerr)

	data := &customErrorData{
		Code:    123,
		Message: "custom message",
	}

	jerr = jerrs.InternalError(data)
	fmt.Println(jerr)

	jsonErr, err := json.Marshal(jerr)
	if err != nil {
		return
	}
	fmt.Println(string(jsonErr))

	// Your custom JSON-RPC error
	jerr = jerrs.NewServerError(-32001)
	fmt.Println(jerr)

	jsonErr, err = json.Marshal(jerr)
	if err != nil {
		return
	}
	fmt.Println(string(jsonErr))

	jerr = jerrs.NewServerError(-32002, nil)
	fmt.Println(jerr)

	jsonErr, err = json.Marshal(jerr)
	if err != nil {
		return
	}
	fmt.Println(string(jsonErr))

	// Your custom JSON-RPC error with your data
	data.Code = 321
	jerr = jerrs.NewServerError(-32002, data)
	fmt.Println(jerr)

	jsonErr, err = json.Marshal(jerr)
	if err != nil {
		return
	}
	fmt.Println(string(jsonErr))

	// Output:
	// JSON-RPC Error: [-32603] Internal error
	// JSON-RPC Error: [-32603] Internal error ("some text")
	// JSON-RPC Error: [-32603] Internal error (null)
	// JSON-RPC Error: [-32603] Internal error ({"code":123,"message":"custom message"})
	// {"code":-32603,"message":"Internal error","data":{"code":123,"message":"custom message"}}
	// JSON-RPC Error: [-32001] Server error
	// {"code":-32001,"message":"Server error"}
	// JSON-RPC Error: [-32002] Server error (null)
	// {"code":-32002,"message":"Server error","data":null}
	// JSON-RPC Error: [-32002] Server error ({"code":321,"message":"custom message"})
	// {"code":-32002,"message":"Server error","data":{"code":321,"message":"custom message"}}
}
