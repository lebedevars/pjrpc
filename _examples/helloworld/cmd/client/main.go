package main

import (
	"context"
	"errors"
	"log"

	"gitlab.com/pjrpc/pjrpc/client"
	"gitlab.com/pjrpc/pjrpc/model"

	"helloworld/proto"
)

func main() {
	cl, err := client.New("http://127.0.0.1:8080/rpc")
	if err != nil {
		log.Fatalf("failed to client.New: %s", err)
	}
	cl.SetLogger(log.Writer()) // Client can write errors about closing the response body.

	rpc := proto.NewHelloWorlderClient(cl)

	resp, err := rpc.Hello(context.Background(), &proto.HelloRequest{Name: "wrong name"})
	if err != nil {
		jerr := &model.ErrorResponse{}
		if errors.As(err, &jerr) {
			log.Println("got json-rpc error:", jerr)
		} else {
			log.Fatalf("got client error: %s", err)
		}
	}

	resp, err = rpc.Hello(context.Background(), &proto.HelloRequest{Name: "error"})
	if err != nil {
		log.Println("got error in error request:", err)
	}

	resp, err = rpc.Hello(context.Background(), &proto.HelloRequest{Name: "panic"})
	if err != nil {
		log.Println("got error in panic request:", err)
	}

	resp, err = rpc.Hello(context.Background(), &proto.HelloRequest{Name: "rpc client"})
	if err != nil {
		log.Fatalf("got error in 3 request: %s", err)
	}

	log.Println("rpc response from:", resp.Name)
}
