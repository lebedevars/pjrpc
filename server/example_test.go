package server_test

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/jerrs"
	"gitlab.com/pjrpc/pjrpc/server"
	"gitlab.com/pjrpc/pjrpc/server/router"
)

type rpc struct{}

type request struct {
	Name string `json:"name"`
}

type response struct {
	Name string `json:"name"`
}

func middleware(next router.Handler) router.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		log.Println("I'm middleware :)")
		return next(ctx, params)
	}
}

func (*rpc) helloHandler(ctx context.Context, params json.RawMessage) (interface{}, error) {
	req := &request{}
	err := json.Unmarshal(params, req)
	if err != nil {
		return nil, jerrs.InvalidParams(err.Error())
	}

	log.Println("request from:", req.Name)
	return &response{Name: "server"}, nil
}

func Example() {
	r := &rpc{}
	srv := server.New()

	// add your error logger to the server to catch error in write response method and panics.
	srv.SetLogger(log.Writer())

	srv.With(middleware)

	srv.RegisterMethod("hello", r.helloHandler)

	mux := http.NewServeMux()
	mux.Handle("/rpc", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Printf("http.ListenAndServe: %s", err)
		return
	}
}
