WASM Example
========

protoc command in `_examples/wasm` dir:

```sh
protoc \
	--go-pjson_out=. --go-pjson_opt=paths=source_relative \
	--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
	proto/wasm.proto
```

run wasm client compiler and run server

```sh
make all
```

Waiting `Starting rpc server on :8080` log message.

Then open URL [http://127.0.0.1:8080/static/](http://127.0.0.1:8080/static/) in browser with F12 debugger.

Switch to Network tab and look `/rpc` request.
