protoc-gen-go-pjson
========

Golang types generator for [pjrpc](https://gitlab.com/pjrpc/pjrpc/) router.

# Install

```sh
go get -u gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjson
```

# Usage

```sh
protoc \
	--go-pjson_out=. --go-pjson_opt=paths=source_relative \
	protodir/file.proto
```

# Flags

`without_omitempty` - generate json tags without omitempty

```sh
--go-pjson_out=without_omitempty=true:. --go-pjson_opt=paths=source_relative \
```
