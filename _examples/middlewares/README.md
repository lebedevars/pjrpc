Middlewares
========

protoc command in `_examples/middlewares` dir:

```sh
protoc \
	--go-pjson_out=. --go-pjson_opt=paths=source_relative \
	--go-pjrpc_out=. --go-pjrpc_opt=paths=source_relative \
	proto/middlewares.proto
```