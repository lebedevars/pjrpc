package genpjrpc

import (
	"strings"

	"google.golang.org/protobuf/compiler/protogen"
)

// MethodInfo is a protogen.Method with names for JSON-RPC router.
type MethodInfo struct {
	Method         *protogen.Method // Origin protogen type
	Name           string           // GoName of the method
	ConstName      string           // Name of the constant with RPC method name
	RPCName        string           // Method name in snake_case may be with service name
	RegHandlerName string           // Name of the handler for registration method in router
}

// ServiceInfo is a protogen.Service with JSON-RPC methods.
type ServiceInfo struct {
	Service *protogen.Service // Origin protogen type
	Name    string            // GoName of service
	Methods []*MethodInfo     // Methods list
}

// GetServiceInfo transforms protogen types with names for JSON-RPC router.
func GetServiceInfo(s *protogen.Service) *ServiceInfo {
	res := &ServiceInfo{
		Service: s,

		Name:    s.GoName,
		Methods: make([]*MethodInfo, len(s.Methods)),
	}

	for i, m := range s.Methods {
		constName := "JSONRPCMethod"
		if MethodsWithServiceName {
			constName += res.Name
		}
		constName += m.GoName

		// MethodName -> ["method", "name"]
		words := splitNameByWords(m.GoName)

		if MethodsWithServiceName {
			// ServiceName -> ["service", "name"] + ["method", "name"]
			words = append(splitNameByWords(res.Name), words...)
		}

		res.Methods[i] = &MethodInfo{
			Method: m,

			Name:           m.GoName,
			RegHandlerName: "reg" + m.GoName,
			ConstName:      constName,
			// ["service", "name"] + ["method", "name"] -> service_name_method_name
			// ["method", "name"] -> method_name
			RPCName: strings.Join(words, "_"),
		}
	}

	return res
}
