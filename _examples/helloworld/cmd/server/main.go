package main

import (
	"context"
	"errors"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/jerrs"
	"gitlab.com/pjrpc/pjrpc/server"

	"helloworld/proto"
)

var errServer = errors.New("server error")

type rpc struct{}

// Hello responder.
func (r *rpc) Hello(ctx context.Context, in *proto.HelloRequest) (*proto.HelloResponse, error) {
	log.Printf("got message from: %s", in.Name)

	switch in.Name {
	case "rpc client": // it's ok, pass
	case "error": // return golang error
		return nil, errServer
	case "panic":
		panic("rpc client sent panic")
	default: // returns json-rpc error
		return nil, jerrs.InvalidParams("wrong name")
	}

	return &proto.HelloResponse{Name: "rpc server"}, nil
}

func main() {
	srv := server.New()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.

	r := &rpc{}

	proto.RegisterHelloWorlderServer(srv, r)

	mux := http.NewServeMux()
	mux.Handle("/rpc", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
