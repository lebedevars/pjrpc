module gitlab.com/pjrpc/pjrpc/cmd/protoc-gen-go-pjson

go 1.15

require (
	github.com/stretchr/testify v1.6.1
	google.golang.org/protobuf v1.25.0
)
