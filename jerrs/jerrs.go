// Package jerrs contains errors of the JSON-RPC specification.
package jerrs

import (
	"encoding/json"

	"gitlab.com/pjrpc/pjrpc/model"
)

func newError(code int, message string, data ...interface{}) *model.ErrorResponse {
	e := &model.ErrorResponse{
		Code:    code,
		Message: message,
		Data:    nil,
	}

	if len(data) != 0 {
		var err error
		e.Data, err = json.Marshal(data[0])
		if err != nil {
			// just don't use bad type in data
			panic("can't json.Marshal error data: " + err.Error())
		}
	}

	return e
}

// ParseError invalid JSON was received by the server.
// An error occurred on the server while parsing the JSON text.
func ParseError(data ...interface{}) *model.ErrorResponse {
	return newError(-32700, "Parse error", data...)
}

// InvalidRequest the JSON sent is not a valid Request object.
func InvalidRequest(data ...interface{}) *model.ErrorResponse {
	return newError(-32600, "Invalid Request", data...)
}

// MethodNotFound the method does not exist / is not available.
func MethodNotFound(data ...interface{}) *model.ErrorResponse {
	return newError(-32601, "Method not found", data...)
}

// InvalidParams invalid method parameter(s).
func InvalidParams(data ...interface{}) *model.ErrorResponse {
	return newError(-32602, "Invalid params", data...)
}

// InternalError internal JSON-RPC error.
func InternalError(data ...interface{}) *model.ErrorResponse {
	return newError(-32603, "Internal error", data...)
}

// NewServerError reserved for implementation-defined server-errors.
// Codes -32000 to -32099.
func NewServerError(code int, data ...interface{}) *model.ErrorResponse {
	return newError(code, "Server error", data...)
}
